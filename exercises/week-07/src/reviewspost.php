<?php
if(!empty($_POST)) {

    $secret = $_ENV['RECAPTCHA_SECRET_KEY'];
    $gRecaptchaResponse = $_POST['g-recaptcha-response'];
    $remoteIp = $_SERVER['REMOTE_ADDR'];

    $recaptcha = new \ReCaptcha\ReCaptcha($secret);
    $resp = $recaptcha->setExpectedHostname('localhost')
        ->verify($gRecaptchaResponse, $remoteIp);

    if ($resp->isSuccess()) {

        $user_name = $_POST['name'];
        $model_id = $modelid;
        $rating = $_POST['rating'];
        $comments = $_POST['comments'];
        //$submitted = date("Y-m-d h:m:s");

        //$stmt = $dbh->prepare('INSERT INTO reviews (user_name, model_id, rating, comments, submitted) VALUES (:user_name, :model_id, :rating, :comments, :submitted)');
        $stmt = $dbh->prepare('INSERT INTO reviews (user_name, model_id, rating, comments) VALUES (:user_name, :model_id, :rating, :comments)');

        $stmt->execute([
            'user_name' => $user_name,
            'model_id' => $model_id,
            'rating' => $rating,
            'comments' => $comments,
            //'submitted' => $submitted,
        ]);
        {
            echo '<div class="alert alert-success alert-dismissible fade show" role="alert">
                        <strong>AWESOME!!</strong> Your Comments Have Been Added!
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">OK</span>
                        </button>
                   </div>';
        }


    } else {
        $errors = $resp->getErrorCodes();
        //var_dump($errors);
        echo '<div class="alert alert-danger alert-dismissible fade show" role="alert">
                <strong>Oh Noooo!!</strong> <a href="#" class="alert-link"  data-bs-toggle="modal" data-bs-target="#modal">Change a few things up</a> and try submitting again.
              </div>';
    }
}