<?php




$modelid = $_GET['id'];


/*
class models
{
    public int $id;
    public string $model;
    public string $description;
    public string $image1;
    public string $image2;
    public string $image3;
    public string $player_level;
    private array $reviews = [];

    public function addReviews(reviews $review): void
    {
        $this->reviews[] = $review;
    }

    public function getRevievs(): array
    {
        return $this->reviews;
    }

}
*/

$stmt = $dbh ->prepare("SELECT * FROM models WHERE id =$modelid");
$stmt->execute();
$pageData = $stmt->fetchAll();



$stmt = $dbh->prepare("SELECT AVG(rating) as averageRating, COUNT(*) FROM reviews WHERE model_id = $modelid ");
$stmt->execute();
$ratingCount = $stmt->fetch();
$averageRating = $ratingCount[0];
$totalReviews = $ratingCount[1];




class reviews

{
    public int $id;
    public string $username;
    public int $rating;
    public int $model_id;
    public string $comments;
    public string $submitted;

}


function hydrateReviews(array $data): reviews
{
    $review = new reviews();
    $review-> id = $data['id'];
    $review-> username = $data['user_name'];
    $review-> model_id = $data['model_id'];
    $review-> rating = $data['rating'];
    $review-> comments = $data['comments'];
    $review-> submitted = $data['submitted'];

    return $review;
}

$stmt = $dbh->prepare("SELECT * FROM reviews WHERE model_id = $modelid ORDER BY DATE(submitted)DESC");
$stmt->execute();
$review = ($stmt->fetchAll());

$hydratedReviews = array_map(static function (array $review): reviews
{
    return hydrateReviews($review);
}, $review);

//var_dump($hydratedReviews);