<?php

require_once '../src/db.php';
require_once '../src/setup.php';
require_once '../public/components/star_rating.php';
require_once '../src/reviews_hydrator.php';
require_once '../src/reviewspost.php';

include '../public/components/reviewModal.php';

$modelid = $_GET['id'];
//MODAL RECAPTCHA AND REVIEWS SECTION

/*if(!empty($_POST)) {

    $secret = '6Le7jGYaAAAAAAhiDlcCaK_yrIfSv-OKPWw4ETXK';
    $gRecaptchaResponse = $_POST['g-recaptcha-response'];
    $remoteIp = $_SERVER['REMOTE_ADDR'];

    $recaptcha = new \ReCaptcha\ReCaptcha($secret);
    $resp = $recaptcha->setExpectedHostname('localhost')
        ->verify($gRecaptchaResponse, $remoteIp);

    if ($resp->isSuccess()) {

        $user_name = $_POST['name'];
        $model_id = $modelid;
        $rating = $_POST['rating'];
        $comments = $_POST['comments'];
        //$submitted = date("Y-m-d h:m:s");

        //$stmt = $dbh->prepare('INSERT INTO reviews (user_name, model_id, rating, comments, submitted) VALUES (:user_name, :model_id, :rating, :comments, :submitted)');
        $stmt = $dbh->prepare('INSERT INTO reviews (user_name, model_id, rating, comments) VALUES (:user_name, :model_id, :rating, :comments)');

        $stmt->execute([
            'user_name' => $user_name,
            'model_id' => $model_id,
            'rating' => $rating,
            'comments' => $comments,
            //'submitted' => $submitted,
        ]);
        {
            echo '<div class="alert alert-success alert-dismissible fade show" role="alert">
                        <strong>AWESOME!!</strong> Your Comments Have Been Added!
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">OK</span>
                        </button>
                   </div>';
        }


    } else {
        $errors = $resp->getErrorCodes();
        //var_dump($errors);
        echo '<div class="alert alert-danger alert-dismissible fade show" role="alert">
                <strong>Oh Noooo!!</strong> <a href="#" class="alert-link"  data-bs-toggle="modal" data-bs-target="#modal">Change a few things up</a> and try submitting again.
              </div>';
    }
}

*/

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <script src="myscript.js"></script>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="stylesheet.css">
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>

    <title>All Things Pearl</title>

</head>

<body id="page">

<div class="container p-1 my-4" id="content">

    <header class="card-header">
        <card class="card-title col-sm-12 text-center mb-5 p-2" id="title">
            <h1 class="text-decoration-underline shadow-sm p-3" id="pagetitle">All Things Pearl</h1>
        </card>
    </header>

<!-- RANGE MENU -->
    <div class="navbar-expand-lg ">
        <?php include '../public/components/navbar.php' ?>
    </div>

    <?php foreach ($pageData as $data): ?>

    <!-- GALLERY -->
    <div class="border border-dark p-5">
        <div class="row col-md-12">
            <div class="col m-1 mb-5">
                <div id="carouselExampleControls" class="carousel slide carousel-dark p-2" data-bs-ride="carousel">
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <a href="#" class="image" data-bs-toggle="modal" data-bs-target="#imagemodal" >
                            <img src="<?php echo $data['image1']?>" class="d-block w-100 img1" alt="<?php echo $data['model']?> 1">
                            </a>
                        </div>
                        <div class="carousel-item">
                            <a href="#" class="image">
                            <img src="<?php echo $data['image2']?>" class="d-block w-100 img1"  alt="<?php echo $data['model']?> 3">
                            </a>
                        </div>
                        <div class="carousel-item">
                            <a href="#" class="image">
                            <img src="<?php echo $data['image3']?>" class="d-block w-100 img1"  alt="<?php echo $data['model']?> 3">
                            </a>
                        </div>
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-bs-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="visually-hidden">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-bs-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="visually-hidden">Next</span>
                    </a>
                </div>
            </div>

            <!-- DESCRIPTION -->
            <div class="col-lg-6 mb-5">

                <h2 class="h2 text-decoration-underline shadow-sm" id="title2">
                    <?php echo $data['model']?>
                </h2>
                <p>
                    <?php echo $data['description']?>
                </p>
                <div class="text-center">
                    <button type="button" class="btn btn-dark p-2 col-lg-6 col-sm-12 shadow-md" data-bs-toggle="modal" data-bs-target="#modal" id="enterbutton">
                        Add A Review
                    </button>
                </div>
            </div>
        </div>
    </div>
<?php endforeach; ?>

    <!-- ADDITIONAL INFO -->
    <div class="">

        <h2 class="h2 shadow-sm mt-4 text-decoration-underline" id="title3">Additional Information</h2>
        <?php foreach ($pageData as $data): ?>
        <table class="table table-striped">

            <tbody>
            <tr>
                <th scope="row">Average Rating</th>
                <td><?php echo star_rating($averageRating)?></td>
            </tr>
            <tr>
                <th scope="row">Player Level</th>
                <td><?php echo $data['player_level']?></td>
            </tr>
            <tr>
                <th scope="row">Total Reviews</th>
                <td><?php echo $totalReviews  ?></td>
            </tr>
            </tbody>
        </table>
        <?php endforeach; ?>
    </div>

                <!-- REVIEWS -->

    <div class="p-2">

        <h2 class="h2 shadow-sm text-decoration-underline text-decoration-underline mt-5 mb-5 col-12" id="title4">Reviews</h2>
        <div class="container text-center mb-5">
            <button class="btn btn-dark " data-bs-toggle="collapse" data-bs-target="#checkins" id="enterbutton">Show/Hide Reviews</button>
        </div>

        <div class="border p-3 bg-light" id="checkin">

            <?php foreach ($review as $reviews): ?>


            <card class="card m-4 border border-light collapse" id="checkins">
                <div class="card-title p-3 ">
                    <h2 class="h4 text-decoration-underline card-title" ><?= $reviews['user_name']?></h2>
                    <span class="card-subtitle"><?php echo star_rating($reviews['rating'])?></span>
                </div>
                <div class="card-body p-3">
                    <p><?= $reviews['comments']  ?></p>
                </div>
                <div class="card-footer bg-white">
                    <p class="text-warning" id="date">
                        <?php
                        try {
                        $newDate = new DateTime($reviews['submitted']);
                        echo $newDate->format('D-d-M-y H:i:s');
                        } catch (Exception $e) {
                        var_dump($e);
                        }
                        ?>
                    </p>
                </div>
            </card>
            <?php endforeach; ?>
        </div>
    </div>

</div>


<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js">

</body>
</html>
