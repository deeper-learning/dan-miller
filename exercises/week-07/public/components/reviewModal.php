<?php
?>

<div class="modal modal-lg" id="modal" tabindex="-1">
    <form  name="myCheckin" id="myCheckin" method="post" action="">
        <div class="modal-dialog">
            <div class="modal-content bg-light">
                <div class="modal-header">
                    <h5 class="modal-title">Review</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body bg-dark">

                    <div class="form-control bg-light">
                        <label for="name" ></label>
                        <input type="text" id="name" name="name" class="w-100" placeholder="Name">
                    </div>
                    <div class="form-control bg-light">
                        <label for="rating">Rating:</label>
                        <select class="form-select" id="rating" name="rating">
                            <option value="" disabled selected>Rating</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                        </select>
                    </div>

                    <div class="form-control bg-light">
                        <label for="comments"></label>
                        <textarea type="text" id="comments" name="comments" class="container" rows="9" placeholder="Comments"></textarea>
                    </div>

                </div>

                <div class=" mt-2 mb-2">
                    <div class="g-recaptcha" data-sitekey=<?php $_ENV['RECAPTCHA_SITE_KEY']?>>></div>
                </div>

                <div class="modal-footer bg-light">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-dark" id="submitBtn" onsubmit="window.location.reload()">Submit</button>
                </div>
            </div>
        </div>
    </form>
</div>

