
/*axios
        .get('http://localhost:8080/checkins')
        .then(resp=> {
            console.log(resp);



             if (resp.data.length> 0){
                 const name=$('<span class="h5 mx-5"></span>').text(resp.data[0].name)
                 const rating=$('<span class="h5 mx-5"></span>').text(resp.data[0].rating)
                 const dateTime=$('<span class="h6 mx-5" ></span>').text(resp.data[0].dateTime)
                 const review=$('<span class="h6 mx-5"></span>').text(resp.data[0].review)
                 $('.name').append(name)
                 $('.rating').append(rating)
                 $('.date').append(dateTime)
                 $('.review').append(review)
             }
         })

 .catch(err => {
     console.log(err);
 })


/*
     resp.data.forEach(checkin => {
                const newH1 = document.createElement('h1');
                newH1.innerText = checkin.rating
                console.log(newH1)
                document.body.appendChild(newH1)
            }
        })
 */


axios.get("http://localhost:8080/checkins")
    .then(function (response) {
        console.log(response);
        if (response.data.length>0) {
            for (let i=0; i < response.data.length; i++) {
                const review = $("<div></div>").addClass("col-12 border border-success p-3 mb-3 bg-white");
                const name = $("<h6 class='h6 text-decoration-underline mb-4'></h6>").text(response.data[i].name).appendTo(review);
                const rating = $("<span id='rating' class='m-4'></span>").text(response.data[i].rating).appendTo(review);
                const comment = $("<p class='p-0 m-4'></p>").text(response.data[i].review).appendTo(review);
                const datetime = $("<p class=''></p>").text(response.data[i].dateTime).appendTo(review);
                $("#checkin").append(review);
            }
        }
    })

