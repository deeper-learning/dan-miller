

axios
    .get('https://api.chucknorris.io/jokes/random')
    .then(addDataToDOM)
    .catch(err => console.log(err))

function addDataToDOM(resp) {
    $('#randomjoke').text(resp.data.value)
}


function refreshpage (){
    window.location.reload();
}