<?php

require_once 'form.php'

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Upload Form</title>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
          rel="stylesheet">



</head>
<body class="container bg-light">
<div class="container w-50">

    <!--Form-->

    <form class="form-control bg-dark" method="post" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']);?>" name="formdata">

        <h1 class="h1 text-white">Details:</h1>

        <div class="form-control text-center bg-light">
            <label for="name">Name:</label>
            <input class="w-50 rounded-pill text-center" type="text" name="name" id="name">
        </div>

        <div class="form-control text-center">
            <label for="dob">D.O.B</label>
            <input class="w-50 rounded-pill text-center" type="date" name="dob" id="dob">
        </div>

        <div class="form-control text-center bg-light">
            <label for="email">Email:</label>
            <input class="w-50 rounded-pill text-center" type="text" name="email" id="email">
        </div>

        <div class="file form-control text-center">
            <label for="picture">Picture:</label>
            <input class="w-50" type="file" name="picture" id="picture">
        </div>

        <div class="form-control text-center bg-light">
            <button class="btn btn-primary w-25 rounded-3" type="submit" value="upload">Submit</button>
        </div>

    </form>








    <!--Carousel-->


    <div id="carouselExampleControls" class="carousel slide mt-5" data-ride="carousel">
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img class="d-block w-100" src="drm1.jpg" alt="First slide">
            </div>
            <div class="carousel-item">
                <img class="d-block w-100" src="drm2.png" alt="Second slide">
            </div>
            <div class="carousel-item">
                <img class="d-block w-100" src="drm5.jpg" alt="Third slide">
            </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>






</div>
<!-- JavaScript Bundle with Popper -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js" integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="script.js"></script>

</body>
</html>
