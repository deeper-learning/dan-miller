<?php

require_once 'db.php';
require_once __DIR__ . '/../vendor/autoload.php';





if(!empty($_POST)) {

    $secret = '6Le7jGYaAAAAAAhiDlcCaK_yrIfSv-OKPWw4ETXK';
    $gRecaptchaResponse = $_POST['g-recaptcha-response'];
    $remoteIp = $_SERVER['REMOTE_ADDR'];

    $recaptcha = new \ReCaptcha\ReCaptcha($secret);
    $resp = $recaptcha->setExpectedHostname('localhost')
        ->verify($gRecaptchaResponse, $remoteIp);

    if ($resp->isSuccess()) {

        $user_name = $_POST['name'];
        $rating = $_POST['rating'];
        $comments = $_POST['comments'];
        $submitted = date("d-m-y H:i:s");


        $stmt = $dbh->prepare('INSERT INTO reviews (user_name, rating, comments, submitted) VALUES (:user_name, :rating, :comments, :submitted)');

        $stmt->execute([
            'user_name' => $user_name,
            'rating' => $rating,
            'comments' => $comments,
            'submitted' => $submitted,
        ]);



    } else {
        $errors = $resp->getErrorCodes();
        var_dump($errors);
    }
}





    $stmt = $dbh->prepare('SELECT * FROM reviews ');
    $stmt->execute();
    $checkin = array_reverse($stmt->fetchAll());

    $stmt = $dbh->prepare('SELECT AVG(rating) as averageRating FROM reviews ');
    $stmt->execute();
    $averageRating = $stmt->fetch();

    $stmt = $dbh->prepare('SELECT COUNT(*) FROM reviews');
    $stmt->execute();
    $totalReviews = $stmt->fetch();

?>





<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Mock Checkin</title>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
          rel="stylesheet">
    <link rel="stylesheet" href="stylesheet.css">
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
    <script src="script.js"></script>

</head>

<body id="page">

<div class="container p-1 my-4 w-75" id="content">


    <header>
        <h1 class="col-sm-12 text-center shadow-sm mb-5" id="title">Lorem Ipsum</h1>
    </header>

    <div class="row m-1 col-md-12">

        <div class="col m-1">
            <div id="carouselExampleControls" class="carousel slide carousel-dark p-2" data-bs-ride="carousel">
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <img src="drm1.jpg" class="d-block w-100"  alt="Drum 1">
                    </div>
                    <div class="carousel-item">
                        <img src="drm2.png" class="d-block w-100" alt="Drum 2">
                    </div>
                    <div class="carousel-item">
                        <img src="drm3.jpeg" class="d-block w-100" alt="Drum 3">
                    </div>
                    <div class="carousel-item">
                        <img src="drm5.jpg" class="d-block w-100" alt="Drum 4">
                    </div>
                </div>
                <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-bs-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="visually-hidden">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-bs-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="visually-hidden">Next</span>
                </a>

            </div>

        </div>



        <div class="col-lg-6">

            <h2 class="h2 text-decoration-underline" id="title2">Lorem Ipsum</h2>

            <p>

                Zombie ipsum reversus ab viral inferno, nam rick grimes malum cerebro. De carne lumbering animata corpora quaeritis. Summus brains sit​​, morbo vel maleficia? De apocalypsi gorger omero undead survivor dictum mauris. Hi mindless mortuis soulless creaturas, imo evil stalking monstra adventus resi dentevil vultus comedat cerebella viventium. Qui animated corpse, cricket bat max brucks terribilem incessu zomby. The voodoo sacerdos flesh eater, suscitat mortuos comedere carnem virus. Zonbi tattered for solum oculi eorum defunctis go lum cerebro. Nescio brains an Undead zombies. Sicut malus putrid voodoo horror. Nigh tofth eliv ingdead.

            </p>

            <button type="button" class="btn btn-primary p-2 col-lg-6 col-sm-12 shadow-md" data-bs-toggle="modal" data-bs-target="#modal" id="checkinBtn">
                Check In
            </button>

        </div>
    </div>


    <div class="modal modal-lg" id="modal" tabindex="-1">
        <form  name="myCheckin" id="myCheckin" method="post" action="">
            <div class="modal-dialog">
                <div class="modal-content bg-light">
                    <div class="modal-header">
                        <h5 class="modal-title">Check In</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body bg-primary">

                        <div class="form-control bg-light">
                            <label for="name"></label>
                            <input type="text" id="name" name="name" class="w-100" placeholder="Name">
                        </div>
                        <div class="form-control bg-light">
                            <label for="rating">Rating:</label>
                            <select class="form-select" id="rating" name="rating">
                                <option value="" disabled selected>Rating</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                            </select>
                        </div>
                        <div class="form-control bg-light">
                            <label for="comments"></label>
                            <textarea type="text" id="comments" name="comments" class="container" rows="9" placeholder="Comments"></textarea>
                        </div>

                    </div>
                    <div class="g-recaptcha" data-sitekey="6Le7jGYaAAAAAHV7ghd1KvyjNLM18V6DJRoCC_kt"></div>

                    <div class="modal-footer bg-light">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary" id="submitBtn">Submit</button>
                    </div>
                </div>
            </div>
        </form>
    </div>

<!-- success -->

    <div class="modal fade" id="modalsuccess" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <h3 class="modal-title">form submitted</h3>
                </div>
            </div>
        </div>
    </div>



    <div class="container">

        <h2 class="h2 shadow-sm mt-4 text-decoration-underline" id="title3">Additional Infromation</h2>

        <table class="table table-striped">

            <tbody>
            <tr>
                <th scope="row">Average Rating</th>
                <td><?php echo $averageRating['averageRating']?></td>
            </tr>
            <tr>
                <th scope="row">Another Statistic</th>
                <td>78/100</td>
            </tr>
            <tr>
                <th scope="row">TOTAL REVIEWS</th>
                <td></td>
            </tr>
            </tbody>
        </table>

    </div>

    <div class="container">

        <h2 class="h2 shadow-sm text-decoration-underline text-decoration-underline mt-5 mb-5 col-12" id="title4">Recent Checkins</h2>

        <div class="container border p-3 bg-light" id="checkin">

            <?php foreach ($checkin as $checkins): ?>
            <card class="card m-4 border border-light">
                <div class="card-title ">
                    <h2 class="h2 text-decoration-underline"><?= $checkins['user_name'] ."&nbsp;&nbsp;&nbsp;&nbsp;". "-". "&nbsp;&nbsp;&nbsp;&nbsp;". $checkins['rating'] ?></h2>
                </div>
                <div>
                    <p><?= $checkins['comments']  ?></p>
                </div>
                <div>
                    <p style="font-size: 10px" class="text-warning"><?= $checkins['submitted'] ?></p>
                </div>
            </card>
            <?php endforeach;  ?>
        </div>
    </div>

</div>






<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="script.js"></script>





</body>
</html>
