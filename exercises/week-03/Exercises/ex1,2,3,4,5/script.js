

$('#form').submit(function (e) {
    e.preventDefault(); //prevents form from default behaviour
    const data = new FormData();
    data.append('number-a', $('#number-a').val())
    data.append('number-b', $('#number-b').val())
    data.append('operation', $('#operation').val())

    axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

    axios
        .post('index.php',data)
        .then(function (response){
            //success
         console.log(response.data);
         $('#answer').val(response.data);
        })
        .catch(function(error){
            //error
            console.log(error);
        })


}) //(e)=event


$('#table tr').click(function (e) {
    console.log(e.target)
    // event.target returns a clicked element (e.g. <td>)
    $(this).children("td").each(function (index) {
        // `this` above points to '#myTable tr'
        // then we select all <td> children
        // each loops through every <td>
        // now `this` points to every <td>
        // eventually, depending on index of <td>
        // in the array of <td>s
        // inject the innerText of a <td>
        // into a chosen/targetted <input> field
        if (index === 0) $('#number-a').val(this.innerText);
        if (index === 1) $('#operation').val(this.innerText);
        if (index === 2) $('#number-b').val(this.innerText);
    })
})
