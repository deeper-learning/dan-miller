

<?php

$result = null;

if(!empty($_POST)){

    $a = $_POST["number-a"];
    $b = $_POST["number-b"];
    $op = $_POST["operation"];

    $date = new DateTime();
    $timestamp = $date->getTimestamp();


    switch($op)
    {
        case 'add':
            $result = $a + $b;
            break;

        case 'subtract':
            $result = $a - $b;
            break;

        case 'multiply':
            $result = $a * $b;
            break;
        case 'divide':
            $result = $a / $b;
            break;
        default:
            $result = "Give Me Something to work with!!";
            break;
    }

    $data = array("number-a"=>$a, "number-b"=>$b, 'operation'=>$op, 'timestamp'=>$timestamp);
   // var_dump($data);

   // $_POST['timestamp'] = $timestamp;
    $serializedCalc= serialize($data);
    file_put_contents($timestamp . '.txt', $serializedCalc);


    echo $result;

    die();

}


?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Form</title>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">

</head>

<body>

<div id="content" class="bg-light container">


                        <!-- MATHS FORM -->

<div id="calculator" class="container bg-success border-5 border-dark text-center w-50 mt-5 pb-5">

    <h1 class="h1 text-center text-decoration-underline text-white shadow">Calculator</h1>

    <form action="" method="post" id="form" class="form-inline bg-light border-5 border-dark" >
        <div class="row">
            <div class="d-inline-block mt-4">
                <label for="number-a"></label>
                <input type="number" class="form-text w-100 p-2 text-center" id="number-a" name="number-a" aria-label="Default select example" placeholder="Number">
            </div>

            <div class="d-inline-block mt-4">
                <label for="operation" style="text-align: center"></label>
                <select class="form-select rounded-end text-center" id="operation" name="operation" aria-label="Default select example">
                    <option selected value="">Select Operation</option>
                    <option value="add" >Add</option>
                    <option value="subtract">Subtract</option>
                    <option value="multiply">Multiply</option>
                    <option value="divide">Divide</option>
                </select>
            </div>

            <div class="d-inline-block mt-4 text-center">
                <label for="number-b"></label>
                <input class="form-text w-100 p-2 text-center" type="number" id="number-b" name="number-b" aria-label="Default select example" placeholder="Number">
            </div>

            <div class="d-inline-block">
                <label for="calculate"></label>
                <button class="btn-dark w-100 p-1 rounded-pill mt-4" id="calculate">Calculate</button>
            </div>

            <div class="d-inline-block">
                <span>=</span><label for="answer"></label>
                <input class="form-control text-center mb-5" id="answer" name="answer" type="text" aria-label="Default select example" readonly value="<?php echo $result ?>">

            </div>
        </div>
    </form>
</div>



                 <!-- PREVIOUS SUBMISSIONS TABLE-->

<div  class="container w-75">

    <h2 class="h2 mt-5 mb-4 text-decoration-underline shadow-lg">Previous Submissions</h2>

    <table id="table" class="table table-striped border-5 border-dark">
        <thead>
        <tr>
            <th>Time</th>
            <th>Value of "A"</th>
            <th>Operation</th>
            <th>Value of "B"</th>
        </tr>
        </thead>

        <tbody>

        <!-- PHP FOR ADDING TO TABLE -->

        <?php
        foreach(glob('*.txt') as $history) {

            $previous = unserialize(file_get_contents($history));

            echo
                '<tr style="cursor: pointer">'
                . '<th>'. date("Y-m-d h:i:sa", $previous["timestamp"]) . '</th>'
                . '<td>'. $previous["number-a"]. '</td>'
                . '<td>'. $previous["operation"] .'</td>'
                . '<td>'. $previous["number-b"]. '</td>'
                . '</tr>';
        }
        ?>

        </tbody>
    </table>

</div>

</div>






                <!-- AXIOS, BOOTSTRAP SCRIPTS -->

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="script.js"></script>


</body>
</html>
