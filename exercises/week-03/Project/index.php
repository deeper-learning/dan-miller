

<?php


class myCheckin
{
    public $name;
    public $rating;
    public $comments;
    public $timestamp;
}

$checkin = new myCheckin();

{

    if (!empty('myCheckin'))
        $checkin->name = $_POST['name'];
    $checkin->rating = $_POST['rating'];
    $checkin->comments = $_POST['comments'];
    $checkin->timestamp = $_POST = date_create("");


    $serializedCheckin = serialize($checkin);
    file_put_contents('$check-ins.txt', $serializedCheckin, FILE_APPEND);
}


?>





<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Mock Checkin</title>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
          rel="stylesheet">


</head>
<body>

<div class="container p-1 my-4 w-75">


    <header>
        <h1 class="col-sm-12 text-center text-decoration-underline shadow-sm">Lorem Ipsum</h1>
    </header>

    <div class="row m-1 col-md-12">

        <div class="col m-1">
            <div id="carouselExampleControls" class="carousel slide carousel-dark p-2" data-bs-ride="carousel">
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <img src="drm1.jpg" class="d-block w-100"  alt="Drum 1">
                    </div>
                    <div class="carousel-item">
                        <img src="drm2.png" class="d-block w-100" alt="Drum 2">
                    </div>
                    <div class="carousel-item">
                        <img src="drm3.jpeg" class="d-block w-100" alt="Drum 3">
                    </div>
                    <div class="carousel-item">
                        <img src="drm5.jpg" class="d-block w-100" alt="Drum 4">
                    </div>
                </div>
                <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-bs-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="visually-hidden">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-bs-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="visually-hidden">Next</span>
                </a>

            </div>

        </div>



        <div class="col-lg-6">

            <h1>Lorem Ipsum</h1>

            <p>

                Zombie ipsum reversus ab viral inferno, nam rick grimes malum cerebro. De carne lumbering animata corpora quaeritis. Summus brains sit​​, morbo vel maleficia? De apocalypsi gorger omero undead survivor dictum mauris. Hi mindless mortuis soulless creaturas, imo evil stalking monstra adventus resi dentevil vultus comedat cerebella viventium. Qui animated corpse, cricket bat max brucks terribilem incessu zomby. The voodoo sacerdos flesh eater, suscitat mortuos comedere carnem virus. Zonbi tattered for solum oculi eorum defunctis go lum cerebro. Nescio brains an Undead zombies. Sicut malus putrid voodoo horror. Nigh tofth eliv ingdead.

            </p>

            <button type="button" class="btn btn-primary p-2 col-lg-6 col-sm-12 shadow-md" data-bs-toggle="modal" data-bs-target="#modal">
                Check In
            </button>

        </div>
    </div>


    <div class="modal modal-lg" id="modal" tabindex="-1">
        <form  name="myCheckin" id="myCheckin" method="post" action="">
            <div class="modal-dialog">
                <div class="modal-content bg-light">
                    <div class="modal-header">
                        <h5 class="modal-title">Check In</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body bg-primary">

                        <div class="form-control bg-light">
                            <label for="name"></label>
                            <input type="text" id="name" name="name" class="w-100" placeholder="Name">
                        </div>
                        <div class="form-control bg-light">
                            <label for="rating">Rating:</label>
                            <select class="form-select" id="rating" name="rating">
                                <option value="" disabled selected>Rating</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                            </select>
                        </div>
                        <div class="form-control bg-light">
                            <label for="comments"></label>
                            <textarea type="text" id="comments" name="comments" class="container" rows="9" placeholder="Comments"></textarea>
                        </div>

                    </div>
                    <div class="modal-footer bg-light">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary" onsubmit="">Submit</button>
                    </div>
                </div>
            </div>
        </form>
    </div>



    <div class="container">

        <h2 class="h2 shadow-sm m-4">Additional Infromation</h2>

        <table class="table">

            <tbody>
            <tr>
                <th scope="row">Average Rating</th>
                <td><img src="stars.png" alt=""></td>
            </tr>
            <tr>
                <th scope="row">Another Statistic</th>
                <td>78/100</td>
            </tr>
            <tr>
                <th scope="row">Yet Another Statistic</th>
                <td>Other Data</td>
            </tr>
            </tbody>
        </table>

    </div>



    <h2 class="h2 shadow-sm text-decoration-underline text-decoration-underline mt-5 mb-5 col-12">Recent Checkins</h2>

    <div class="container border p-3 bg-light" id="checkin">
        <?php
        foreach(glob('*.txt') as $history) {

            $previous = unserialize(file_get_contents($history));



        }
        ?>

    </div>








</div>




<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="script.js"></script>





</body>
</html>
