axios.get("http://localhost:8080/checkins")
    .then(function (response) {
            console.log(response);


                    if (response.data.length>0) {
                    for (let i=0; i < response.data.length; i++) {
                            const review = $("<div></div>").addClass("col-12 border border-success p-3 mb-3 bg-white");
                            const name = $("<h6 class='h3 text-decoration-underline mb-4'></h6>").text(response.data[i].name).appendTo(review);
                            const rating = $("<span id='rating' class='m-4'></span>").text(response.data[i].rating).appendTo(review);
                            const comment = $("<p class='p-0 m-4 h-5'></p>").text(response.data[i].review).appendTo(review);
                            const datetime = $("<p class=''></p>").text(response.data[i].dateTime).appendTo(review);
                            $("#checkin").append(review);
                        }
                }
        })