<?php

require_once '../src/userpost.php';


?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Lobster&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="stylesheet.css">
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>

    <title>All Things Pearl</title>

</head>

<body id="page">

<div class="container p-1 my-4" id="content">

    <header class="card-header">
        <card class="card-title col-sm-12 text-center mb-5 p-2" id="title">
            <h1 class="text-decoration-underline shadow-sm p-3" id="pagetitle">All Things Pearl</h1>
        </card>
    </header>

    <div class="navbar-expand-lg ">
        <?php include 'components/navbar.php' ?>
    </div>


    <div class="card text-center bg-light">
        <h2 class="card-header text-decoration-underline fa" id="title2">Register</h2>
    </div>

    <!-- Register Form -->

    <div class="card border border-light p-2">
        <form class="row g-3" method="post" action="">
            <div class="col-lg w-50">
                <label for="regname" class="form-label"></label>
                <input type="text" class="form-control" id="regname" name="regname" placeholder="Name"  required>
            </div>
            <div class="col-lg w-50">
                <label for="regemail" class="form-label"></label>
                <input type="email" class="form-control" id="regemail" name="regemail" placeholder="Email Address"  required>
            </div>
            <div class="col-lg w-50">
                <label for="regusername" class="form-label"></label>
                <div class="input-group">
                    <span class="input-group-text" id="inputGroupPrepend2">@</span>
                    <input type="text" class="form-control" id="regusername" name="regusername" placeholder="Username"  aria-describedby="inputGroupPrepend2" required>
                </div>
            </div>
            <div class="col-lg w-50">
                <label for="regpassword" class="form-label"></label>
                <input type="password" class="form-control" id="regpassword" name="regpassword" placeholder="Password" required>
            </div>

            <div class="col-12 text-center p-5">
                <button class="btn btn-dark w-50 fa p-3" type="submit" id="registerButton">Register</button>
            </div>
        </form>
    </div>

</div>




        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
        <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js">

</body>
</html>


