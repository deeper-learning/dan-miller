<?php

require_once '../src/db.php';
require_once '../src/setup.php';


?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Lobster&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="stylesheet.css">
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>

    <title>All Things Pearl</title>

</head>

<body id="page">

<div class="container p-1 my-4" id="content">

    <header class="card-header">
        <card class="card-title col-sm-12 text-center mb-5 p-2" id="title">
            <h1 class="text-decoration-underline shadow-sm p-3" id="pagetitle">All Things Pearl</h1>
        </card>
    </header>

    <!-- RANGE MENU -->
    <div class="navbar-expand-lg ">
        <?php include 'components/navbar.php' ?>
    </div>

    <!-- Login -->


        <div class="container">
            <div class="col-md-6">
                <div class="card">
                    <form class="box">
                        <h1>Login</h1>
                        <p class="text-muted"> Please enter your login and password!</p>
                        <label for="loginuser"></label>
                        <input type="text" name="loginUser" placeholder="Username" id="loginuser">
                        <label for="loginPassword"></label>
                        <input type="password" name="loginPassword" placeholder="Password" id="loginPassword">
                        <input type="submit" name="submitbtn" value="Login">
                        <div class="col-md-12">
                        </div>
                    </form>
                </div>
            </div>

    </div>



</div>




<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js">
    </body>
    </html>


