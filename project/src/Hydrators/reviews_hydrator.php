<?php




$modelid = $_GET['id'];

$stmt = $dbh ->prepare("SELECT * FROM models WHERE id =$modelid");
$stmt->execute();
$pageData = $stmt->fetchAll();

$stmt = $dbh->prepare("SELECT AVG(rating) as averageRating, COUNT(*) FROM reviews WHERE model_id = $modelid ");
$stmt->execute();
$ratingCount = $stmt->fetch();
$averageRating = $ratingCount[0];
$totalReviews = $ratingCount[1];


class Review

{
    public int $id;
    public string $username;
    public int $rating;
    public int $model_id;
    public string $comments;
    public string $submitted;

}


function hydrateReviews(array $data): Review
{
    $review = new Review();
    $review-> id = $data['id'];
    $review-> username = $data['user_name'];
    $review-> model_id = $data['model_id'];
    $review-> rating = $data['rating'];
    $review-> comments = $data['comments'];
    $review-> submitted = $data['submitted'];

    return $review;
}

$stmt = $dbh->prepare("SELECT * FROM reviews WHERE model_id = $modelid ORDER BY DATE(submitted)DESC");
$stmt->execute();
$reviews = $stmt->fetchAll();

$hydratedReviews = array_map(static function (array $review): Review
{
    return hydrateReviews($review);
}, $reviews);

//var_dump($hydratedReviews);
