-- MySQL dump 10.13  Distrib 8.0.22, for macos10.15 (x86_64)
--
-- Host: localhost    Database: allThingsPearl
-- ------------------------------------------------------
-- Server version	5.7.33

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `reviews`
--

DROP TABLE IF EXISTS `reviews`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `reviews` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(45) NOT NULL,
  `model_id` int(10) NOT NULL,
  `rating` int(5) NOT NULL,
  `comments` varchar(3000) NOT NULL,
  `submitted` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reviews`
--

LOCK TABLES `reviews` WRITE;
/*!40000 ALTER TABLE `reviews` DISABLE KEYS */;
INSERT INTO `reviews` VALUES (20,'Dan Miller',1,3,'test hydration','2021-03-07 13:24:37'),(21,'Dan Miller',1,2,'test hydration 2','2021-03-07 13:39:00'),(22,'Dan Miller',2,3,'test hydration 1','2021-03-07 15:10:48'),(23,'Dan Miller',3,3,'test hydration','2021-03-07 15:13:11'),(24,'Dan Miller',3,5,'test hydration 2','2021-03-07 15:14:03'),(25,'Dan Miller',3,4,'test hydration 3','2021-03-07 15:17:04'),(26,'Dan Miller',3,3,'test hydration 4','2021-03-07 15:17:36'),(27,'Dan Miller',3,3,'test success','2021-03-07 15:25:33'),(28,'Dan Miller',4,4,'test success','2021-03-07 15:26:42'),(29,'Dan Miller',1,3,'test success','2021-03-07 15:39:25'),(30,'Dan Miller',1,4,'test success 5','2021-03-07 15:48:20'),(31,'Dan Miller',5,3,'test ','2021-03-07 15:54:17'),(32,'Dan Miller',5,4,'test 2','2021-03-07 15:55:14'),(33,'Dan Miller3',5,3,'test 4','2021-03-07 15:56:01'),(34,'Dan Miller',1,3,'test success','2021-03-07 15:57:33'),(35,'Dan Miller',1,2,'test success','2021-03-07 15:59:21'),(36,'Dan Miller',1,3,'test','2021-03-07 16:03:43'),(37,'Dan Miller',1,3,'test ','2021-03-07 16:05:13'),(38,'test1',4,4,'test','2021-03-07 16:06:44'),(39,'Dan Miller3',4,3,'test','2021-03-07 16:07:54'),(40,'Dan Miller',3,2,'test','2021-03-07 16:10:19'),(41,'Dan Miller3',4,2,'test','2021-03-07 16:11:42'),(42,'ad',3,1,'ad','2021-03-07 16:12:34'),(43,'bbbbbbb',1,3,'bbbb','2021-03-07 16:14:11'),(44,'bbbbbbb',5,1,'bbbbbb','2021-03-07 16:15:47'),(45,'cccccc',5,4,'cccccc','2021-03-08 09:34:03'),(46,'ddddddd',5,3,'ddddd','2021-03-08 09:35:10'),(47,'ddddddd',1,3,'ddddddd','2021-03-08 18:55:11'),(48,'ddddddd',2,3,'dddddd','2021-03-08 19:09:17'),(49,'eeeeeee',2,4,'eeeeeeee','2021-03-08 19:10:33'),(50,'ffffff',2,3,'ffff','2021-03-08 19:11:14'),(51,'ggggggg',2,3,'gggggggg','2021-03-08 19:11:41'),(52,'Dan Miller',1,3,'testing','2021-03-08 19:48:18');
/*!40000 ALTER TABLE `reviews` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-03-21 17:51:24
