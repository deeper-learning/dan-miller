-- MySQL dump 10.13  Distrib 8.0.22, for macos10.15 (x86_64)
--
-- Host: localhost    Database: allThingsPearl
-- ------------------------------------------------------
-- Server version	5.7.33

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `models`
--

DROP TABLE IF EXISTS `models`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `models` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `model` varchar(45) NOT NULL,
  `description` varchar(3000) NOT NULL,
  `image1` varchar(300) NOT NULL,
  `image2` varchar(300) NOT NULL,
  `image3` varchar(300) NOT NULL,
  `player_level` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `models`
--

LOCK TABLES `models` WRITE;
/*!40000 ALTER TABLE `models` DISABLE KEYS */;
INSERT INTO `models` VALUES (1,'Masters Series','The Masters Series has always been synonymous with the very best in professional instruments for the touring drummer. Masters Maple Complete drums present a refined representation of over two decades of Masters drums in one kit. Its road ready touches make it reliable for consistent sound and performance from venue to venue at a price that won\'t make you forfeit your per diem. Masters Maple Complete drums are available in five premium finishes that represent your uncompromised aesthetic. Choose from transparent and metallic gloss lacquers, classic White Marine Pearl covering, or the custom \"Cain & Abel\" graphic finish featuring our industry exclusive Artisan II treatment. Masters Maple Complete gives you the freedom of building your ideal kit around four available shell packs and an expanded selection of component drums.','https://pearldrum.com/sites/default/files/image_folder/PRODUCTS/DRUMSETS/MASTER_MAPLE_COMPLETE/GALLERY/MCT925XEP_840_full.jpg','https://pearldrum.com/sites/default/files/image_folder/PRODUCTS/DRUMSETS/MASTER_MAPLE_COMPLETE/GALLERY/MCT924_427-full.jpg','https://pearldrum.com/sites/default/files/image_folder/PRODUCTS/DRUMSETS/MASTER_MAPLE_COMPLETE/GALLERY/MCT923XSP_837-FT.jpg','Professional'),(2,'Session Series','The Session Series is the next step in Pearl\'s proud 20-year Session legacy. Shells in this series are built from Masters-grade hard birch with furniture-grade African mahogany for gorgeous tone and designer looks to match. Shells come crowned with Masterworks-inspired 60-degree outer bearing edges and SuperHoop IIs for a crisp attack and focused midrange to balance an enveloping low end. Session players will marvel at this lineup\'s pre-EQed tone, especially under close mics, while players and audiences of all backgrounds will be drawn to the Pearl Session Studio Select\'s breathtaking visuals. Available in several stunning lacquered and Delmar USA-wrapped finishes. The Session Studio Select features shell technology borrowed from the Masters series. Thin, resonant 5.4mm EvenPly-Six (6-ply) shells are comprised of 4-ply birch outers, for focus and cut live or in the studio, along with two inner plies of furniture-grade African mahogany for warmth, balance, and designer good looks that stare back at you from behind the kit. ','https://pearldrum.com/sites/default/files/image_folder/PRODUCTS/DRUMSETS/SESSION%20STUDIO%20SELECT/GALLERY/805x500%20STS925XSPC766%20Expanded.jpg','https://pearldrum.com/sites/default/files/image_folder/PRODUCTS/DRUMSETS/SESSION%20STUDIO%20SELECT/GALLERY/805x500%20STS925XSPC405.jpg','https://pearldrum.com/sites/default/files/image_folder/PRODUCTS/DRUMSETS/SESSION%20STUDIO%20SELECT/GALLERY/805x500%20STS925XSPC112%20Close.jpg','Intermediate/Professional'),(3,'Crystal Beat','A Shell pack with a difference, the Crystal Beat from Pearl may look thin and light but the tone is the exact opposite.  Acrylic Drums have a unique attack, more forceful that heavy wood shells, acrylic gives you a tone that a lot of bands just love. Thanks largely to a one-of-a-kind drummer, John Bonham, who used a set in the 1976 Led Zeppelin concert film, The Song Remains the Same.If you need a drum tone that wont ever get lost in the mix then this could be your ideal setup. Worried about thin tones? don\'t be. Theyâ€™re less ringy, more focused and have great presence. There are not a whole lot of harmonics youâ€™re dealing with, and the volumeâ€™s all up in the drum. You don\'t have to hit them super-hard, because youâ€™ll have a lot of ambient volume onstage anyway. If your guitarist has an amp that goes to 11, well, now you can hold your own. Take the same care with them as you would with wooden drums and these will sound great year after year, gig after gig','https://pearldrum.com/sites/default/files/image_folder/PRODUCTS/DRUMSETS/CRYSTAL%20BEAT/Gallery/805x500%20CRB525FPC732.jpg','https://pearldrum.com/sites/default/files/image_folder/PRODUCTS/DRUMSETS/CRYSTAL%20BEAT/Gallery/805-500_CRB525FPC-731-RUBY-RED.jpg','https://pearldrum.com/sites/default/files/image_folder/PRODUCTS/DRUMSETS/CRYSTAL%20BEAT/Gallery/805x500%20CRB524FPC733.jpg','Intermediate/Professional'),(4,'Decade Series','Pearls 70 years of drum craftsmanship culminate with the Decade Maple kit. Whether this is your first kit or the upgrade you\'ve been looking for the Decade range deserves your attention. With 5.4mm Maple shells, pro-level features, and gorgeous lacquer finishes, Decade helps elevate your performance at a price that was previously impossible. Some Maple drums use a combo of maple ply and \"filler\" material. These drums are constructed form 6 plys of solid, quality maple. While the change in sound might be subtle the strength and build quality is noticeably better. Combine this with the design of the low-mass/low-contact shells you end up with a drum set that can takle the rigours of the road without sacrificing tone or resonance.','https://pearldrum.com/sites/default/files/image_folder/PRODUCTS/DRUMSETS/DECADE%20MAPLE/GALLERY/DMP926S_262_tom.jpg','https://pearldrum.com/sites/default/files/image_folder/PRODUCTS/DRUMSETS/DECADE%20MAPLE/Finishes/DMP%20207%20Ultramarine%20Velvet1.jpg','https://pearldrum.com/sites/default/files/image_folder/PRODUCTS/DRUMSETS/DECADE%20MAPLE/805x500%20DMP925SPC260%20Full.jpg','Intermediate'),(5,'Export Series','The arrival of the Pearl Export in 1982 changed forever the drum kit market. Here was a top quality entry-level kit that set a new benchmark. Within a couple of years every other manufacturer was vying to compete.  The original Export shaped almost every generic starter kit for almost 30 years. Pearl had already shipped one million Export kits by 1995! Each year the spec was improved until eventually Pearl brought out the Forum to restake its starter position.  Then in 2007 the Export name itself was dropped in favour of Vision, while Target took up the starter slot. So you had Target-Forum-Vision. Now Pearl has re-introduced the Export name and dumped Forum, so we have Target-Export-Vision. It may be the budget kit in Pearl\'s range, but it is what we always expected from the Export moniker, a bargain kit with the latest refinements. This is not just a re-badged Forum. No, we have upgraded shells, new lugs, new tom bracket and a superb, improved hardware package.','https://pearldrum.com/sites/default/files/image_folder/PRODUCTS/DRUMSETS/EXPORT_EXL/FINISHES/EXL_217_Rasperry%20Sunset1.jpg','https://pearldrum.com/sites/default/files/image_folder/PRODUCTS/DRUMSETS/EXPORT_EXL/FINISHES/EXL_219_Indigo%20Night2.jpg','https://pearldrum.com/sites/default/files/image_folder/PRODUCTS/DRUMSETS/EXPORT_EXL/FINISHES/EXL_218_Ember%20Dawn2.jpg','Beginner');
/*!40000 ALTER TABLE `models` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-03-21 17:51:24
